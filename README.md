Symfony Sample of Code from SymfonyArt team
========================
This is parts of Symfony code from different projects.

What's inside?
--------------

Some samples:

  * Our code style

  * Model
  
  * Form
  
  * Event listener
  
  * Tests

Contact information
--------------

[SymfonyArt Team](http://symfonyart.com)

Alexander Nemanov: alex.n@symfonyart.com
