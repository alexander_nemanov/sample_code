<?php

namespace AppBundle;

/**
 * Class AppEvents
 * @package AppBundle
 */
final class AppEvents
{
    /**
     *  The app.create_user event is thrown when new User object is created.
     */
    const APP_CREATE_USER = 'app.create_user';

    /**
     * The event is thrown when new instance of AppBundle\Entity\HomeUser class has been created
     * Event class \AppBundle\Event\HomeUserEvent
     */
    const APP_HOME_USER_CREATED = 'app.home_user.created';

    /**
     * Event class \AppBundle\Event\xxx\PushNotificationEvent
     */
    const APP_EVENT_PUSH_NOTIFICATION = 'app.event.push_notification';
}
