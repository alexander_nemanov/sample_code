<?php

namespace AppBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use AppBundle\Service\xxx\Exception\BadRequestException;
use AppBundle\Service\Translator;


/**
 * Class RestExceptionListener
 * @package AppBundle\EventListener
 */
class RestExceptionListener
{
    const STATUS_ERROR = 'error';
    const STATUS_OK = 'ok';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var string[]
     */
    private $domains;

    public function __construct(LoggerInterface $logger, Translator $translator, $environment, $domains)
    {
        $this->logger = $logger;
        $this->translator = $translator;
        $this->environment = $environment;
        $this->domains = $domains;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($this->environment != 'prod') {
            return;
        }

        $request = $event->getRequest();

        if (!in_array($request->getHost(), $this->domains)) {
            return null;
        }

        $exception = $event->getException();
        $message = 'rest.error';

        if ($exception instanceof BadRequestException) {
            if ($decodedMessage = json_decode($exception->getMessage(), true)) {
                $message = $decodedMessage['message'];
            }
        }

        $this->logger->critical($exception->getMessage());
        $this->logger->critical($exception->getTraceAsString());

        $message = $this->translator->trans($message);

        $responseData = [
            'status'  => self::STATUS_ERROR,
            'message' => $message,
        ];

        $response = new JsonResponse($responseData);
        $response->headers->set('X-Status-Code', 200);
        $event->setResponse($response);
    }
}
