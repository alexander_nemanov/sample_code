<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints\KxxxEmailUnique;

/**
 * Class BaseUserType
 * @package AppBundle\Form
 */
class BaseUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'common.first_name.blank',
                    ]),
                ],
            ])
            ->add('lastName', null, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'common.last_name.blank',
                    ]),
                ],
            ])
            ->add('email', null, [
                'required' => true,
                'disabled' => !$options['isCreate'],
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'common.email.blank',
                    ]),
                    new Assert\Email(),
                    new KxxxEmailUnique()
                ],
            ])
            ->add('phonePrefix', PhoneCountryPrefixType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'common.phone_prefix.blank',
                    ]),
                    new Assert\Length([
                        'min' => 0,
                        'max' => 8,
                    ]),
                ]
            ])
            ->add('phone', null, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'common.phone.blank',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $options)
    {
        $options->setDefaults([
            'inherit_data' => true,
            'isCreate' => true
        ]);
    }
}
