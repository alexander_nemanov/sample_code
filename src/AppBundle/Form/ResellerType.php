<?php

namespace AppBundle\Form;

use AppBundle\Datek\Helper\UserHelper;
use AppBundle\Entity\Reseller;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ResellerType extends AbstractType
{
    /**
     * @var UserHelper
     */
    protected $helper;

    /**
     * ResellerType constructor.
     *
     * @param UserHelper $helper
     */
    public function __construct(UserHelper $helper)
    {
        $this->helper = $helper->useAdminServices();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $partners = $this->helper->getPartners();

        $builder
            ->add('partnerId', ChoiceType::class, [
                'label' => 'Partner',
                'choices' => array_flip($partners),
                'choices_as_values' => true
            ])
            ->add('partnerName', HiddenType::class)
            ->add('base_user', BaseUserType::class, [
                'data_class' => Reseller::class,
                'isCreate' => $options['isCreate'],
            ])
            ->add('resetPassword', ResetPasswordType::class, [
                'data_class' => Reseller::class,
                'isCreate' => $options['isCreate'],
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($partners) {
            $data = $event->getData();
            $partnerId = $data['partnerId'];
            $data['partnerName'] = $partners[$partnerId];
            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reseller::class,
            'isCreate' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return 'reseller';
    }
}
