<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints\DirectUserPassword;
use AppBundle\Validator\Constraints\KxxxPasswordFormat;


/**
 * Class ResetPasswordType
 * @package AppBundle\Form
 */
class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isCreate = $options['isCreate'];

        if (!$isCreate) {
            $builder
                ->add('currentPassword', PasswordType::class, [
                    'mapped' => false,
                    'required' => false,
                    'constraints' => [
                        new DirectUserPassword([
                            'groups' => ['edit'],
                        ]),
                    ],
                ])
            ;
        }

        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => 'password',
                'required' => $isCreate,
                'options' => ['translation_domain' => 'FOSUserBundle'],
                'first_options' => ['label' => ucfirst(($isCreate ? '' : 'new ') . 'password')],
                'second_options' => ['label' => ucfirst('repeat ' . ($isCreate ? '' : 'new ') . 'password')],
                'invalid_message' => 'fos_user.password.mismatch',
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'fos_user.password.blank',
                        'groups' => ['create'],
                    ]),
                    new KxxxPasswordFormat([
                        'groups' => ['create', 'edit'],
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $options)
    {
        $options->setDefaults([
            'inherit_data' => true,
            'isCreate' => true,
            'validation_groups' => function (FormInterface $form) {
                $isCreate = $form->getConfig()->getOption('isCreate', true);
                $groups = [];

                if ($isCreate) {
                    $groups[] = 'create';
                } else {
                    if (!$form->offsetExists('plainPassword')) {
                        return $groups;
                    }

                    $viewData = $form->get('plainPassword')->getViewData();
                    $plainPassword = null;

                    if (is_array($viewData) && array_key_exists('first', $viewData)) {
                        $plainPassword = $viewData['first'];
                    }

                    // if user tries to change password in edit form, then add constraint to validate password
                    if ($plainPassword) {
                        $groups[] = 'edit';
                    }
                }

                return $groups;
            }
        ]);
    }
}
