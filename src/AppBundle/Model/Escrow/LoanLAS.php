<?php

namespace ProjectName\FrontendBundle\Model\Escrow;


class LoanLAS
{
    private static $PAYMENTS_PER_YEAR = 12;

    /**
     * @var double
     */
    private $loanAmount;

    /**
     * Annual interest rate
     * @var double
     */
    private $rate;

    /**
     * Loan period in month
     * @var int
     */
    private $loanPeriod;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var double
     */
    private $scheduledMonthlyPayment;

    /**
     * @var array[\ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment]
     */
    private $payments;

    /**
     * The SF loan id
     * @var string
     */
    private $loanId;

    /**
     * The SF investor id
     * @var string
     */
    private $investorId;

    /**
     * The SF business id
     * @var string
     */
    private $businessId;


    public function __construct()
    {
        $this->payments = array();
        $this->investorId = null;
        $this->businessId = null;
    }

    /**
     * Return true ih this business las
     * @return bool
     */
    public function isBusinessLAS()
    {
        return (null !== $this->businessId) ? true : false;
    }

    /**
     * @param string $businessId
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setBusinessId($businessId)
    {
        $this->businessId = $businessId;
        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessId()
    {
        return $this->businessId;
    }

    /**
     * @param string $investorId
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setInvestorId($investorId)
    {
        $this->investorId = $investorId;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvestorId()
    {
        return $this->investorId;
    }

    /**
     * @param string $loanId
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setLoanId($loanId)
    {
        $this->loanId = $loanId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoanId()
    {
        return $this->loanId;
    }

    /**
     * @return int
     */
    public function getNumberPaymentsPerYear()
    {
        return static::$PAYMENTS_PER_YEAR;
    }

    /**
     * @param float $loanAmount
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setLoanAmount($loanAmount)
    {
        $this->loanAmount = $loanAmount;
        return $this;
    }

    /**
     * @return float
     */
    public function getLoanAmount()
    {
        return $this->loanAmount;
    }

    /**
     * @param int $loanPeriod
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setLoanPeriod($loanPeriod)
    {
        $this->loanPeriod = $loanPeriod;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoanPeriod()
    {
        return $this->loanPeriod;
    }

    /**
     * @param \array[\ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment] $payments
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setPayments($payments)
    {
        $this->payments = array();
        // Order payments by payment number
        /** @var \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment $payment */
        foreach ($payments as $payment) {
            $this->addPayment($payment);
        }

        ksort($this->payments);

        return $this;
    }

    /**
     * @param \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment $payment
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    private function addPayment(LoanLASPayment $payment)
    {
        $this->payments[$payment->getNumber()] = $payment;
        return $this;
    }

    /**
     * @return \array[]
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * Get loan payment by his number
     * @param int $number
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment|null
     */
    public function getPayment($number)
    {
        if (!isset($this->payments[$number])) {
            return null;
        }

        return $this->payments[$number];
    }

    /**
     * @return int
     */
    public function getPaymentsCount()
    {
        return count($this->payments);
    }

    /**
     * @param float $rate
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @param bool $relative
     * @return float
     */
    public function getRate($relative = false)
    {
        return (true == $relative) ? $this->rate / 100 : $this->rate;
    }

    /**
     * @param float $scheduledMonthlyPayment
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setScheduledMonthlyPayment($scheduledMonthlyPayment)
    {
        $this->scheduledMonthlyPayment = $scheduledMonthlyPayment;
        return $this;
    }

    /**
     * @return float
     */
    public function getScheduledMonthlyPayment()
    {
        return $this->scheduledMonthlyPayment;
    }

    /**
     * @param \DateTime $startDate
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    public function setStartDate($startDate)
    {
        if (null == $startDate) {
            $this->startDate = null;
        } else {
            $this->startDate = clone $startDate;
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

}
