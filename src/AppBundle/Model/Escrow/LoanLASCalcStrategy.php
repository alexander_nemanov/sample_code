<?php

namespace ProjectName\FrontendBundle\Model\Escrow;


class LoanLASCalcStrategy
{
    /**
     * @param \ProjectName\FrontendBundle\Model\Escrow\LoanLAS $las
     * @return float
     */
    public function calcSchedulePayment(LoanLAS $las)
    {
        return $this->calculate(
            $las->getRate(true),
            $las->getNumberPaymentsPerYear(),
            $las->getLoanPeriod(),
            $las->getLoanAmount()
        );
    }

    /**
     * @param $rate
     * @param $numberPaymentsPerYear
     * @param $loanPeriod
     * @param $loanAmount
     * @return float
     */
    private function calculate($rate, $numberPaymentsPerYear, $loanPeriod, $loanAmount)
    {
        $ratePercent = $rate;
        $ratePerPayment = $ratePercent / $numberPaymentsPerYear;
        $val1 = $ratePerPayment * pow(1 + $ratePerPayment, $loanPeriod);
        $val2 = pow(1 + $ratePerPayment, $loanPeriod) - 1;

        return (0 == $val2) ? 0 : round($loanAmount * $val1 / $val2, 2);
    }
}
