<?php

namespace ProjectName\FrontendBundle\Model\Escrow;


/**
 * Class LoanLASFactory
 * @service_name: pn_frontend.model.escrow.loan_lass.factory
 * @package ProjectName\FrontendBundle\Model\Escrow
 */
class LoanLASFactory
{
    /**
     * @param \ProjectName\FrontendBundle\Model\Escrow\LoanInfo $loanInfo
     * @param \ProjectName\FrontendBundle\Model\Escrow\Bid $bid
     * @return LoanLAS
     */
    public function factoryForBid(LoanInfo $loanInfo, Bid $bid)
    {
        return $this->create(
            $bid->getAmount(),
            $bid->getRate(),
            $loanInfo->getTerm(),
            $loanInfo->getLASFirstPaymentDate(),
            $loanInfo->getId()
        );
    }

    /**
     * @param \ProjectName\FrontendBundle\Model\Escrow\LoanInfo $loanInfo
     * @return LoanLAS
     */
    public function factory(LoanInfo $loanInfo)
    {
        return $this->create(
            $loanInfo->getEscrowAmount(),
            $loanInfo->getAverageLoanRate(),
            $loanInfo->getTerm(),
            $loanInfo->getLASFirstPaymentDate(),
            $loanInfo->getId()
        );
    }

    /**
     * @param float $amount
     * @param float $rate
     * @param float $term
     * @param \DateTime $firstPaymentDate
     * @param string $loanId The loan Id
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    private function create($amount, $rate, $term, \DateTime $firstPaymentDate, $loanId)
    {
        $las = new LoanLAS();

        $las->setLoanAmount($amount)
            ->setRate($rate)
            ->setLoanPeriod($term)
            ->setStartDate($firstPaymentDate)
            ->setLoanId($loanId)
        ;

        $loanLASCalcStrategy = new LoanLASCalcStrategy();
        $las->setScheduledMonthlyPayment($loanLASCalcStrategy->calcSchedulePayment($las));

        // Create payments
        $paymentsStrategy = new LoanLASPaymentStrategy();
        $payments = $paymentsStrategy->calculate($las);
        $las->setPayments($payments);

        return $las;

    }
}
