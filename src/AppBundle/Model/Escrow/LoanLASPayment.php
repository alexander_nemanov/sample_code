<?php

namespace ProjectName\FrontendBundle\Model\Escrow;


class LoanLASPayment
{
    /**
     * @var int
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $paymentDate;

    /**
     * @var double
     */
    private $beginBalance;

    /**
     * @var double
     */
    private $scheduledMonthlyPayment;

    /**
     * @var double
     */
    private $totalPayment;

    /**
     * @var double
     */
    private $principal;

    /**
     * @var double
     */
    private $interest;

    /**
     * @var double
     */
    private $endingBalance;

    /**
     * @var double
     */
    private $cumulativeInterest;

    /**
     * @var string
     */
    private $investorName;


    /**
     * @param string $name
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setInvestorName($name)
    {
        $this->investorName = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvestorName()
    {
        return $this->investorName;
    }

    /**
     * @param float $beginBalance
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setBeginBalance($beginBalance)
    {
        $this->beginBalance = $beginBalance;
        return $this;
    }

    /**
     * @return float
     */
    public function getBeginBalance()
    {
        return $this->beginBalance;
    }

    /**
     * @param float $cumulativeInterest
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setCumulativeInterest($cumulativeInterest)
    {
        $this->cumulativeInterest = $cumulativeInterest;
        return $this;
    }

    /**
     * @return float
     */
    public function getCumulativeInterest()
    {
        return $this->cumulativeInterest;
    }

    /**
     * @param float $endingBalance
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setEndingBalance($endingBalance)
    {
        $this->endingBalance = $endingBalance;
        return $this;
    }

    /**
     * @return float
     */
    public function getEndingBalance()
    {
        return $this->endingBalance;
    }

    /**
     * @param float $interest
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;
        return $this;
    }

    /**
     * @return float
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * @param int $number
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param \DateTime $paymentDate
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param float $principal
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * @param float $scheduledMonthlyPayment
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setScheduledMonthlyPayment($scheduledMonthlyPayment)
    {
        $this->scheduledMonthlyPayment = $scheduledMonthlyPayment;
        return $this;
    }

    /**
     * @return float
     */
    public function getScheduledMonthlyPayment()
    {
        return $this->scheduledMonthlyPayment;
    }

    /**
     * @param float $totalPayment
     * @return \ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment
     */
    public function setTotalPayment($totalPayment)
    {
        $this->totalPayment = $totalPayment;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPayment()
    {
        return $this->totalPayment;
    }

}
