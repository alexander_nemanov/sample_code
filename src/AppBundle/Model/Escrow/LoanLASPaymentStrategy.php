<?php

namespace ProjectName\FrontendBundle\Model\Escrow;


class LoanLASPaymentStrategy
{
    /**
     * @var \ProjectName\FrontendBundle\Model\Escrow\LoanLAS
     */
    private $las;

    /**
     * @var array[\ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment]
     */
    private $payments;

    /**
     * @var int
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $paymentDate;

    /** @var double */
    private $beginBalance;

    /**
     * @var double
     */
    private $monthlyInterestRate;


    public function __construct()
    {
        $this->payments = array();
    }

    /**
     * @param LoanLAS $las
     * @return array[\ProjectName\FrontendBundle\Model\Escrow\LoanLASPayment]
     */
    public function calculate(LoanLAS $las)
    {
        $this->las = $las;

        $this->monthlyInterestRate = $this->las->getRate(true) / $this->las->getNumberPaymentsPerYear();
        $this->beginBalance = $las->getLoanAmount();
        $this->paymentDate = $las->getStartDate();

        for ($this->number = 1; $this->number <= $las->getLoanPeriod(); $this->number++) {
            $payment = $this->calcPayment();

            $this->payments[$payment->getNumber()] = $payment;

            $this->beginBalance = $payment->getEndingBalance();
            $this->paymentDate->modify('+1 month');
        }

        return $this->payments;
    }

    /**
     * @return LoanLASPayment
     */
    private function calcPayment()
    {
        $payment = new LoanLASPayment();
        $payment
            ->setNumber($this->number)
            ->setPaymentDate(clone $this->paymentDate)
            ->setBeginBalance($this->beginBalance)
            ->setScheduledMonthlyPayment($this->las->getScheduledMonthlyPayment())
        ;

        // Total payment
        $payment->setTotalPayment(
            ($payment->getScheduledMonthlyPayment() < $payment->getBeginBalance()) ? $payment->getScheduledMonthlyPayment() : $payment->getBeginBalance()
        );

        // Interest
        $payment->setInterest(
            round(
                $payment->getBeginBalance() * $this->monthlyInterestRate,
                2
            )
        );

        // Principal
        $payment->setPrincipal(
            $payment->getTotalPayment() - $payment->getInterest()
        );

        // Ending balance
        $payment->setEndingBalance(
            ($payment->getScheduledMonthlyPayment() < $payment->getBeginBalance()) ? $payment->getBeginBalance() - $payment->getPrincipal() : 0
        );

        return $payment;
    }
}
