<?php

namespace ProjectName\FrontendBundle\Tests\Model\Escrow;

use ProjectName\FrontendBundle\Model\Escrow\LoanLASCalcStrategy;
use ProjectName\FrontendBundle\Model\Escrow\LoanLAS;


class LoanLASCalcStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testRateZero()
    {
        $las = $this->getMock('ProjectName\FrontendBundle\Model\Escrow\LoanLAS');
        $las->expects($this->once())
            ->method('getRate')->will($this->returnValue(0))
        ;
        $las->expects($this->once())
            ->method('getNumberPaymentsPerYear')->will($this->returnValue(12))
        ;
        $las->expects($this->exactly(1))
            ->method('getLoanPeriod')->will($this->returnValue(12))
        ;
        $las->expects($this->any())
            ->method('getLoanAmount')->will($this->returnValue(21000))
        ;

        $strategy = new LoanLASCalcStrategy();
        $val = $strategy->calcSchedulePayment($las);

        $this->assertEquals(0, $val);
    }

    public function testCalcSchedulePayment()
    {
        $strategy = new LoanLASCalcStrategy();

        $this->assertEquals(2193.57, $strategy->calcSchedulePayment($this->getLAS(50000, 24, 5)));
        $this->assertEquals(1384.35, $strategy->calcSchedulePayment($this->getLAS(30000, 24, 10)));
        $this->assertEquals(3577.27, $strategy->calcSchedulePayment($this->getLAS(80000, 24, 6.875)));
    }

    private function getLAS($amount, $paymentsPerYear, $rate)
    {
        $las = new LoanLAS();
        $las->setLoanAmount($amount)
            ->setLoanPeriod($paymentsPerYear)
            ->setRate($rate)
        ;

        return $las;
    }
}
