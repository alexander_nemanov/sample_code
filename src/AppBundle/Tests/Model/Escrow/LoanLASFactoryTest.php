<?php

namespace ProjectName\FrontendBundle\Tests\Model\Escrow;

use ProjectName\FrontendBundle\Model\Escrow\LoanLASFactory;


class LoanLASFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testFactory()
    {
        $loanInfo = $this->getMock('ProjectName\FrontendBundle\Model\Escrow\LoanInfo');
        $loanInfo->expects($this->once())->method('getEscrowAmount');
        $loanInfo->expects($this->once())->method('getAverageLoanRate');
        $loanInfo->expects($this->once())->method('getTerm');
        $loanInfo->expects($this->once())->method('getTerm');
        $loanInfo->expects($this->once())
            ->method('getLASFirstPaymentDate')->will($this->returnValue(new \DateTime()))
        ;
        $loanInfo->expects($this->once())->method('getId');

        $factory = new LoanLASFactory();
        $las = $factory->factory($loanInfo);

        $this->assertInstanceOf('ProjectName\FrontendBundle\Model\Escrow\LoanLAS', $las);
    }
}
