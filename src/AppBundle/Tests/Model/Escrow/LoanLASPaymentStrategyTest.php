<?php

namespace ProjectName\FrontendBundle\Tests\Model\Escrow;

use ProjectName\FrontendBundle\Model\Escrow\LoanLAS;
use ProjectName\FrontendBundle\Model\Escrow\LoanLASPaymentStrategy;


class LoanLASPaymentStrategyTest extends \PHPUnit_Framework_TestCase
{
    private $testData = array(
        1 => array(
            'amount' => 80000,
            'period' => 24,
            'rate' => 6.875,
            'scheduledPayment' => 3577.27,
            'paymentDate' => '2014-07-01',
        ),
        2 => array(
            'amount' => 50000,
            'period' => 24,
            'rate' => 5,
            'scheduledPayment' => 2193.57,
            'paymentDate' => '2014-07-01',
        ),
        3 => array(
            'amount' => 30000,
            'period' => 24,
            'rate' => 10,
            'scheduledPayment' => 1384.35,
            'paymentDate' => '2014-07-01',
        ),
    );

    public function testEndingBalance()
    {
        $results = array(
            1 => array(
                1 => 76881.06,
                23 => 3557.01,
//                23 => 3556.9,1
                24 => 0,
            ),
            2 => array(
                1 => 48014.76,
//                23 => 2184.47,
                24 => 0,
            ),
            3 => array(
                1 => 28865.65,
//                23 => 1372.91,
                24 => 0,
            ),
        );

        for ($i = 1; $i <= 2; $i++) {
            $las = $this->createLASFromTestData($i);

            $strategy = new LoanLASPaymentStrategy();
            $payments = $strategy->calculate($las);

            foreach($results[$i] as $k => $v) {
                $payment = $payments[$k];
                $this->assertEquals($v, $payment->getEndingBalance());
            }
        }
    }

    public function testPaymentsCountAndDate()
    {
        $startPaymentDate = new \DateTime('2014-07-01');
        $las = $this->createLASFromTestData(1);

        $strategy = new LoanLASPaymentStrategy();
        $payments = $strategy->calculate($las);

        $this->assertEquals(24, count($payments));

        $payment = $payments[1];
        $this->assertEquals($startPaymentDate->format('Y-m-d'), $payment->getPaymentDate()->format('Y-m-d'));

        $payment = $payments[24];
        $lastDate = $startPaymentDate->modify('+'.(24 - 1).' month');
        $this->assertEquals($lastDate->format('Y-m-d'), $payment->getPaymentDate()->format('Y-m-d'));
    }

    public function testInterest()
    {
        $las = $this->createLASFromTestData(1);

        $strategy = new LoanLASPaymentStrategy();
        $payments = $strategy->calculate($las);

        $payment = $payments[1];
        $this->assertEquals(458.33, $payment->getInterest());

        $payment = $payments[24];
        $this->assertEquals(20.38, $payment->getInterest());

        $las = $this->createLASFromTestData(2);

        $strategy = new LoanLASPaymentStrategy();
        $payments = $strategy->calculate($las);

        $payment = $payments[1];
        $this->assertEquals(208.33, $payment->getInterest());

        $payment = $payments[24];
        $this->assertEquals(9.1, $payment->getInterest());

        $las = $this->createLASFromTestData(3);

        $strategy = new LoanLASPaymentStrategy();
        $payments = $strategy->calculate($las);

        $payment = $payments[1];
        $this->assertEquals(250, $payment->getInterest());

        $payment = $payments[24];
        $this->assertEquals(11.44, $payment->getInterest());
    }

    private function createLASFromTestData($number)
    {
        return $this->createLAS(
            $this->testData[$number]['amount'],
            $this->testData[$number]['period'],
            $this->testData[$number]['rate'],
            $this->testData[$number]['scheduledPayment'],
            new \DateTime($this->testData[$number]['paymentDate'])
        );
    }

    private function createLAS($amount, $period, $rate, $scheduledPayment, \DateTime $paymentDate)
    {
        $las = new LoanLAS();
        $las->setLoanAmount($amount)
            ->setLoanPeriod($period)
            ->setRate($rate)
            ->setScheduledMonthlyPayment($scheduledPayment)
            ->setStartDate($paymentDate)
        ;

        return $las;
    }
}
